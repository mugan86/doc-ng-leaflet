import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MarkdownPreviewComponent } from './components/markdown-preview/markdown-preview.component';
import { menuData } from './routesPaths';
console.log(menuData)

const paths = [
  {
    path: '',
    component: MarkdownPreviewComponent
  }
];
menuData.forEach((__, index) => {
  menuData[index].content.forEach((menu) => {
    let path = menu.path;
    if (menu.path.indexOf('marker') !== 0 && menu.path.includes) {
      path = `markers${path.substring(1)}`
    }
    paths.push({
      path,
      component: MarkdownPreviewComponent
    })
  })
})

const routes: Routes = [
  {
    path: '',
    component: MarkdownPreviewComponent
  },
  {
    path: 'demos-ejemplos.md',
    component: MarkdownPreviewComponent
  },
  {
    path: 'aspectos-basicos/mapa-basico.md',
    component: MarkdownPreviewComponent
  },
  {
    path: "aspectos-basicos/personalizar-tamano.md",
    component: MarkdownPreviewComponent
  },
  {
    path: "aspectos-basicos/centrar-en-ubicacion.md",
    component: MarkdownPreviewComponent
  },
  {
    path: "zoom/positions-btn-texts.md",
    component: MarkdownPreviewComponent
  },
  {
    path: "zoom/levels.md",
    component: MarkdownPreviewComponent
  },
  {
    path: "markers/intro.md",
    component: MarkdownPreviewComponent
  },
  {
    path: "markers/one-marker.md",
    component: MarkdownPreviewComponent
  },
  {
    path: "markers/two-or-more-markers.md",
    component: MarkdownPreviewComponent
  },
  {
    path: "markers/random-markers-default-zoom.md",
    component: MarkdownPreviewComponent
  },
  {
    path: "markers/random-markers-select-zoom.md",
    component: MarkdownPreviewComponent
  },
  {
    path: "one-marker.md",
    component: MarkdownPreviewComponent
  },
  {
    path: "two-or-more-markers.md",
    component: MarkdownPreviewComponent
  },
  {
    path: "random-markers-default-zoom.md",
    component: MarkdownPreviewComponent
  },
  {
    path: "random-markers-select-zoom.md",
    component: MarkdownPreviewComponent
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

