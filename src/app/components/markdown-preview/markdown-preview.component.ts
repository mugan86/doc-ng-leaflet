import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MarkdownService } from 'ngx-markdown';
import { map } from 'rxjs/internal/operators/map';

@Component({
  selector: 'app-markdown-preview',
  templateUrl: './markdown-preview.component.html',
  styleUrls: ['./markdown-preview.component.css']
})
export class MarkdownPreviewComponent implements OnInit {
  markdown: string | undefined;

  constructor(private mdService: MarkdownService, private http: HttpClient, private router: Router) { }
  
  async ngOnInit() {
    console.log('url', this.router.url);
    let selectFile = (this.router.url === '/') ? `/readme.md`: this.router.url;
    if (selectFile.indexOf('/marker') === -1) {
      console.log(selectFile.indexOf('marker'))
      selectFile = `/markers/${selectFile}`
    }
    const markdownRaw = await this.http.get(`/i18n/es${selectFile}`, 
      { responseType: 'text' }).toPromise() || '';   
    this.markdown = this.mdService.compile(markdownRaw);
  }

}
