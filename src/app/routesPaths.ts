export const menuData = [
    {
        "label": "Introducción",
        "content": [
            {
                "label": "Instalaciones y configuraciones",
                "path": "",
            },
            {
                "label": "Demos / Ejemplos",
                "path": "demos-ejemplos.md",
            }
        ]
    },
    {
        "label": "Aspectos Básicos",
        "content": [
            {
                "label": "Mapa Básico",
                "path": "aspectos-basicos/mapa-basico.md",
            },
            {
                "label": "Tamaño Personalizado mapa",
                "path": "aspectos-basicos/personalizar-tamano.md",
            },
            {
                "label": "Centrar en ubicación especificada",
                "path": "aspectos-basicos/centrar-en-ubicacion.md",
            }
        ]
    },
    {
        "label": "Zoom",
        "content": [
            {
                "label": "Posiciones y textos botones",
                "path": "zoom/positions-btn-texts.md",
            },
            {
                "label": "Nivel seleccionado",
                "path": "zoom/levels.md",
            },
        ]
    },
    {
        "label": "Marcadores",
        "content": [
            {
                "label": "Introducción",
                "path": "markers/intro.md"
            },
            {
                "label": "Añadir un marcador",
                "path": "markers/one-marker.md"
            },
            {
                "label": "Añadir varios marcadores",
                "path": "markers/two-or-more-markers.md"
            },
            {
                "label": "Aleatorios con zoom por defecto",
                "path": "markers/random-markers-default-zoom.md"
            },
            {
                "label": "Aleatorios con zoom selección",
                "path": "markers/random-markers-select-zoom.md"
            }
        ]
    }
]